

import React from "react";

import Header from '../organisms/Header'
import Card from '../molecules/Card'

export default function HomePage({information}) {
  console.log("app data")
  console.log(information);
  const Cards= information.map((item) =>{
    return <Card key={item.id} data={item} />
  });
  return (
    <div className="home-page">
      <Header />
      <section className="recomended">
      <h1 className="title">Recommended</h1>
      <div className="grid">{Cards}</div>
      </section>
      
    </div>
  );
}
