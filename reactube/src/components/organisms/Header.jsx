import React from "react";
import logoLight from '../../assets/images/logo-light.svg'
export default function Header (){
return <header className={"header"}>
<a href="#">
  <img src={logoLight} alt="Logo" />
</a>

<div className="search-bar">
  <input placeholder="Search" />
  <a href="#">Search</a>
</div>
</header>


}
 