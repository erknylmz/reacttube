import React from 'react'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'

import HomePage from './components/template/HomePage'
import VideoPage from './components/template/VideoPage'

import information from './information.json'
import './css/style.css'

export default function App() {
  
  return (
    <Router>
        <div className="App">
        <Switch>
          <Route 
            path="/" 
            exact
            render={() => <HomePage information={information} />}
           />
           <Route 
           path="/video/:id" 
           render = { ({match} ) => <VideoPage match={match} information= {information[0]} />}
           />
           </Switch>
        </div>
    </Router>
  );
}

